package com.example.dell.calculatrice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {
    private MainActivity main ;

    @Before
    public void setUp(){
        main= new MainActivity();
    }

    @Test
    public void addition() {
        double res = main.addition(2,2);
        assertEquals(4.0,res,.1);
    }
}